import $ from 'jquery';
import {MoviesService} from '../services/movies.service';

const POPULAR_MOVIES_ENDPOINT = '/discover/movie';
const STATIC_IMG_PATH = 'https://image.tmdb.org/t/p/w500/';

export class MoviesComponent {

  constructor() {
    this.moviesService = new MoviesService()
  }

  // Implement createMoviesList method:
  createMovieList() {
    // 1. Create an (jquery) HTML UL Element
    // 2. use moviesService to get your movies
    // 3. loop over de movies array from the service and create an (jquery) LI element
    //    with the movie title as text
    // 4. append to our UL element
    // 5. return the UL element
  }
}
