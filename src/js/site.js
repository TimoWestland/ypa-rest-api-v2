import $ from 'jquery';
import scss from '../css/site.css';

import {MoviesComponent} from './components/movies.component';

$(function () {
  const moviesComponent = new MoviesComponent();
  // Use moviesComponent to get a list of movies
  // then $('body).append() the movies list
});
